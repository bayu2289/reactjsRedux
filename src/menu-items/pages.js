// assets
import { IconKey, IconReceipt2, IconBug, IconBellRinging, IconPhoneCall, IconBrandPocket } from '@tabler/icons';

// constant
const icons = {
    IconKey: IconKey,
    IconReceipt2: IconReceipt2,
    IconBug: IconBug,
    IconBellRinging: IconBellRinging,
    IconPhoneCall: IconPhoneCall,
    IconBrandPocket: IconBrandPocket
};

//-----------------------|| EXTRA PAGES MENU ITEMS ||-----------------------//

export const pages = {
    id: 'pages',
    title: 'Pages',
    caption: 'Pages Caption',
    type: 'group',
    children: [
        {
            id: 'authentication',
            title: 'Authentication',
            type: 'collapse',
            icon: icons['IconKey'],
            children: [
                {
                    id: 'login3',
                    title: 'Login',
                    type: 'item',
                   url: '/pages/login/login3',
                   target: true
                },
                
                {
                    id: 'register3',
                    title: 'Register',
                    type: 'item',
                    url: '/pages/register/register3',
                    target: true
                }
            ]
            
        },
        {
            id: 'authentication',
            title: 'Produk',
            type: 'collapse',
            icon: icons['IconBrandPocket'],
            children: [
                {
                    id: 'ProdukList',
                    title: 'Produk List',
                    type: 'item',
                    url: '/tes',
                    
                    breadcrumbs: false
                },
                {
                    id: 'ProdukKategori',
                    title: 'Kategori List',
                    type: 'item',
                    url: '/produkKategori',
                    
                    breadcrumbs: false
                },
                {
                    id: 'register3',
                    title: 'Register',
                    type: 'item',
                    url: '/pages/register/register3',
                    target: true
                }
            ]
            
        },
       
    ]
};
