import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import React from 'react'

const useStyles = theme => ({
    root: {
       // top: theme.spacing(10)
       top: theme.spacing(1)
    }
  })

function notification(props) {
    const {notif,setNotif} = props;
    const { classes } = props;
    const handleClose = (event,reason)=>{
        if(reason=='clickaway'){
            return;
        }
        setNotif({
            ...notif,
            isOpen:false
        })
    }
    return (
       <Snackbar
            className={classes.root}
           open={notif.isOpen}
            autoHideDuration={3000}
            anchorOrigin={{vertical:'top',horizontal:'right'}}
            onClose={handleClose}
            >
           <Alert 
           variant="filled"
           severity={notif.type} 
            onClose={handleClose}>
                {notif.message}
           </Alert>

       </Snackbar>
    )
}
notification.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(useStyles)(notification);