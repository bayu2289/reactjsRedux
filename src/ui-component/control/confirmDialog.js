import { Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Typography, Button } from '@material-ui/core'
import React from 'react'
import Controls from "../control"
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import NotListedLocationIcon from '@material-ui/icons/NotListedLocation';

const useStyles = theme => ({
    dialog: {
      padding:theme.spacing(2),
      position:'absolute',
      top: theme.spacing(5),
      textAlign: 'center'
    },
    dialogContent: {
        textAlign: 'center'
    },
    dialogAction: {
        justifyContent:'center'
    },
    titleIcon: {
       // backgroundColor: theme.palette.primary.light,
        color: theme.palette.secondary.main,
        '&:hover': {
            backgroundColor: theme.palette.secondary.light,
            cursor: 'default'
        },
        '& .MuiSvgIcon-root': {
            fontSize: '8rem',
        }
    }

  })

function confirmDialog(props) {
  //  console.log(props)
    const { classes } = props;
    const {confirmDialog,setConfirmDialog} = props
    return (
        <Dialog open={confirmDialog.isOpen} classes={{paper:classes.dialog}}>
            <DialogTitle>
                {/* <IconButton disableRipple>
                    <NotListedLocationIcon />
                </IconButton> */}
                 <IconButton disableRipple className={classes.titleIcon}>
                    <NotListedLocationIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent className={classes.dialogContent}>
                <Typography variant="h6">
                    {confirmDialog.title}
                </Typography>
                <Typography variant="subtitle2">
                    {confirmDialog.subTitle}
                </Typography>
            </DialogContent>
            <DialogActions className={classes.dialogAction}>
                <Controls.Button
                text="No"
               // color="default"
                onClick={()=>setConfirmDialog({ ...confirmDialog, isOpen:false})}/>
                 <Controls.Button
                text="Yes"
                color="secondary"
                onClick={confirmDialog.onConfirm}/>
            </DialogActions>
        </Dialog>
    )
}
confirmDialog.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(useStyles)(confirmDialog);