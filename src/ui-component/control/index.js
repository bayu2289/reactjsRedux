import Input from "./input";
import TablePaggination from "./useTable"
import ActionButton from "./actionButton"
import Notification from "./notification"
import ConfirmDialog from './confirmDialog'
import Button from './button'
import popup from './popup'


const controls ={
    Input,
    TablePaggination,
    ActionButton,
    Button,
    Notification,
    ConfirmDialog,
    popup
}
export default controls;
