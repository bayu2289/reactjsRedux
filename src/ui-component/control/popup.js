import React from 'react'
import { Dialog, DialogContent, DialogTitle, Typography } from '@material-ui/core'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import control from '../control'; 
import CloseIcon from '@material-ui/icons/Close'

const useStyles = theme => ({
    dialogWrapper: {
      padding: theme.spacing(2),
      position: 'absolute',
      top: theme.spacing(0)
    },
    dialogTitle:{
        paddingRight:'0px'
    }
  })

function popup(props) {
    //const classes = useStyles();
    const { classes } = props;
    const{title, children, openPopup,setOpenPopup,size} = props;
    return (
      <div>
       <Dialog open={openPopup} maxWidth="lg" fullWidth maxWidth={size} classes={{paper:classes.dialogWrapper}}>
          <DialogTitle className={classes.dialogTitle}>
              <div style={{display:'flex'}}>
                  <Typography variant="h4" component="div" style={{flexGrow:1}}
                  >{title}</Typography>
                    <control.ActionButton
                    color="secondary"
                    onClick={()=>{setOpenPopup(false)}}
                   // variant ="outlined"
                
                    >
                    <CloseIcon />
                    </control.ActionButton>
              </div>
          </DialogTitle> 
          <DialogContent dividers>
               {children}
          </DialogContent>
       </Dialog>
       </div>
    )
}
popup.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(useStyles)(popup);