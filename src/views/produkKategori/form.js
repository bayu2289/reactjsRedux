import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/styles';
import Paper from '@material-ui/core/Paper';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Control from './../../ui-component/control';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import * as LocalStorege from './../../store/localStorage'
import Api from './../../axios/Api'
import * as getData from './../../axios/getData'
import { useSelector , useDispatch } from 'react-redux'
import { setKategori } from '../../redux/actions/kategoriAction';

const validationSchema = yup.object({
    namaProduk: yup
      .string('Enter your Nama Produk')
      .required('Nama Produk is required'),
 
  });

  const useStyles = makeStyles((theme) => ({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 400,
      },
    },
    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
      },
      formControl: {
        margin: theme.spacing(1),
        minWidth: 400,
      },
  }));

 const Form = (props) => {
    const classes = useStyles();
    const editkategori = useSelector((state) => state.allKategori.editkategori)
    const dispatch = useDispatch()
    const [values,setValues] = useState('');
    const [dataKategori,setDataKategori] = useState([]);
    const [notif, setNotif] = useState({isOpen:false, message:'',type:''})
    const {afterSave,setAfterSave} = props;

    const addKategori = async (value) => {
      console.log(value)
      await Api.post('addkategori/', {
         id : value.id == undefined ? 0:value.id,
         nama_kategori: value.namaProduk.toUpperCase()
      },
      {
        headers: {
        'Content-Type': 'application/json',
        'Accept':'application/json',
        'Authorization': 'Bearer ' + LocalStorege.getTokenAuth()
      }
     }
     )
      .then((res) => {
        //console.log(res)
 
        //setOpenPopup(false)
        setAfterSave(true)
 
  
      })
      .catch((error) => {
        console.error(error)
        console.log(error)
      })

    }



    useEffect(() => {
      
      
        //getDataKategori();
        //getData.getDataKategori()
        
        console.log("edit",editkategori)
      
     },[]);

    console.log("tes",props)
    const formik = useFormik({
        initialValues: {
          namaProduk: editkategori.nm_kategori,
          id: editkategori.id,
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            console.log(values.id)
            addKategori(values)
          //alert(JSON.stringify(values, null, 2));
        },
      });

      const handleInputChange = e => {
      
        const {name,value} = e.target
        setValues({
          ...values,
          [name] : value
        })
       
       }
    return (
        <div>
          <form className={classes.root} onSubmit={formik.handleSubmit}>
          <Paper className={classes.paper}>
          <TextField
            fullWidth
            id="namaProduk"
            name="namaProduk"
            label="Nama Kategori"
            value={formik.values.namaProduk}
            onChange={formik.handleChange}
            error={formik.touched.namaProduk && Boolean(formik.errors.namaProduk)}
            helperText={formik.touched.namaProduk && formik.errors.namaProduk}
            />

     
         
           
         
        
         

          </Paper>
          <Paper className={classes.paper}>
                
          <Button color="primary" variant="contained" type="submit">
            Submit
            </Button>
          </Paper>
       
        </form>
        <Control.Notification
         notif={notif}
         setNotif={setNotif}
         />
        </div>
    )
}
export default Form;