import React, { useEffect, useState, useCallback } from 'react';

// material-ui
import { InputAdornment } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteIcon from '@material-ui/icons/Delete';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';

//ui-componen
import Control from './../../ui-component/control';
import useTable from './../../ui-component/control/useTable';
// import ConfirmDialog from './../../ui-component/control/confirmDialog'


//server
import { useSelector , useDispatch } from 'react-redux'
import { searchProduct, setProducts } from './../../redux/actions/productActions'
import { setKategori,selectedKategori, searchKategori } from './../../redux/actions/kategoriAction'
import Api from './../../axios/Api'
import * as getdata from './../../axios/getData'
import * as LocalStorege from './../../store/localStorage'


// project imports
import MainCard from '../../ui-component/cards/MainCard';

//form
import FormProduct from './form';

//==============================|| SAMPLE PAGE ||==============================//
const useStyles = makeStyles(theme => ({
  searchIcon: {
    position: 'absolute',
    top: '20%',
    left: '0',
    marginTop: '-24px',
    color: 'rgba(0,0,0,.87)'
  },
  searchInput: {
      width: '75%'
    },
    datess: {

      margin: theme.spacing(1)
    },
  newButton: {
      position: 'absolute',
      left: '300px'
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'left',
      color: theme.palette.text.secondary,
    },
    paperButtonNew: {
      padding: theme.spacing(1),
      textAlign: 'right',
      color: theme.palette.text.secondary,
    },
    pencarian: {
      flexGrow: 1,
      marginBottom: '25px',
    },

}));

const ProdukKategori = () => {
  const products = useSelector((state) => state.allKategori.kategori)
  
  const token = useSelector((state) => state.users.user[0].token)
  const [valuesSearch,setValuesSearch] = useState('');
  const [notif, setNotif] = useState({isOpen:false, message:'',type:''})
  const[confirmDialog,setConfirmDialog] = useState({isOpen:false, title: '', subTitle: ''})
  const[openPopup,setOpenPopup] = useState(false)
  const[afterSave,setAfterSave] = useState(false)
  const [allDataProduct,setAllDataProduct] = useState([]);
  const[filterFn,setFilterFn] = useState({fn: items => {return items;}});
  const [titleForm,setTitleForm] = useState("New Produk");
  const dispatch = useDispatch()
  const classes = useStyles();

  const deleteKategori = async (value) => {
    console.log(value)
    await Api.post('deletekategori/', {
       id : value.id == undefined ? 0:value.id,
       nama_kategori: value.namaProduk
    },
    {
      headers: {
      'Content-Type': 'application/json',
      'Accept':'application/json',
      'Authorization': 'Bearer ' + LocalStorege.getTokenAuth()
    }
   }
   )
    .then((res) => {
      setAfterSave(true)


    })
    .catch((error) => {
      console.error(error)
      console.log(error)
    })

  }

  const getDataProduct = async () => {
   // console.log(LocalStorege.getTokenAuth());
    //await ApiStaging.get('get-item-produk', {
    await Api.get('kategori/', {
      headers: {
        'Content-Type': 'application/json',
        'Accept':'application/json',
        'Authorization': 'Bearer ' + LocalStorege.getTokenAuth()
      },
    //  params: {
     //   branch_id: 1,
     //   page: 1
    //  }
    })
    .then((res) => {
 
      //
      console.log(res);
      setAllDataProduct(res.data)
      //setAllDataProduct(res.data.return.data)
     // LocalStorege.copyFromDb(products)
      console.log("lewat get data server");

    })
    .catch((error) => {
      console.error(error)
    })
}
 
  useEffect(() => {

    if(allDataProduct.length == 0){
      getDataProduct();
      console.log(allDataProduct);
     }
  
    if(valuesSearch.valuesSearch == undefined){
      dispatch(setKategori(allDataProduct))
      setFilterFn({
        fn:items => {
            // else
             return items = allDataProduct;
        }
       })
    }
    else{
      dispatch(searchKategori(allDataProduct,valuesSearch.valuesSearch))
      setFilterFn({
        fn:items => {
            // else
            if(valuesSearch.valuesSearch==""){
              return items = allDataProduct
            }
            else{
              return items = products;
            }
            
        }
       })
    }

  

    if(afterSave == true){
     
    //  window.location.reload(false);
   // addTodo()
  
      setOpenPopup(false)
      setAfterSave(false)
      setNotif({
        isOpen: true,
        message: "Success",
        type: 'success'
      })
      getDataProduct()
    }

    


  },[valuesSearch,afterSave,allDataProduct]);

  const callback = useCallback(() => {    
    dispatch(setKategori())
    console.log('Clicked!');  
    console.log(products)
   
  
  }, []);

 
  const handleChange = (e) => {
    let target = e.target;
    setValuesSearch({valuesSearch: target.value});
   // dispatch(searchProduct(products,valuesSearch.valuesSearch))

    //dispatch(searchProduct(products,valuesSearch.valuesSearch))
   // console.log(valuesSearch.valuesSearch)
  }

  const headCells =[

    {id:'id',lable:'SN', align:"left"},
    {id:'name',lable:'Name',align:"left"},

    {id:'action',lable:'Action',disableSorting:true,align:"center"},
  ]

  const {
    TblContainer,
    TblHead,
    TblPagination,
    recordsAfterPagingAndSort
  }=useTable(allDataProduct,headCells,filterFn);

  const onDelete = value =>{
    // if(window.confirm('Are you sure to delete this record')){
    setConfirmDialog({
        ...confirmDialog,
        isOpen:false
    })
    deleteKategori(value)
 
 
  }

  const openInPopup = item => {

    if(item.id !=0){
    setTitleForm(`Edit Produk : ${item.id}`)
    }
    console.log("edit",item)
    dispatch(selectedKategori(item))

    setOpenPopup(true)

  }
 //console.log('produk',allDataProduct.products)
  
    return (
  
    <MainCard title="Kategori List">
      <div className={classes.pencarian}>
      <Grid container spacing={3}>
   
        <Grid item xs={6}>
          <Paper className={classes.paper}>
          <Control.Input
                name = "cariProduk"
                label= "Search Produk"
                 className={classes.searchInput}
                    InputProps={{
                        startAdornment:(<InputAdornment position="start">
                        
                        </InputAdornment>)
                    }}
                    onChange={handleChange}

               //onChange={handleSearchform}
            />
          </Paper>
        </Grid>
        <Grid item xs={6}>
          <Paper className={classes.paperButtonNew}>
          <Button variant="contained" color="primary"  
          onClick ={()=>{
            setTitleForm("New Kategori")
            dispatch(selectedKategori(""))
            setOpenPopup(true)
            }}>
                  New Kategori
                </Button>
          </Paper>
        </Grid>
     
      </Grid>
    </div>
  
      <TblContainer component={Paper}>
        <Table aria-label="simple table">
        <TblHead/>
          <TableBody>
          {recordsAfterPagingAndSort().map( row => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.id}
                </TableCell>
                <TableCell align="left">{row.nm_kategori}</TableCell>
                <TableCell align='center'>
         
                <Control.ActionButton
                    color="primary"
                    onClick ={()=>{openInPopup(row)}}>
                      <EditOutlinedIcon fontSize="small" />
                </Control.ActionButton>                              
                <Control.ActionButton
                    color="primary"
                    onClick ={()=>{
                      setConfirmDialog({
                        isOpen:true,
                        title:`Are you sure to delete ${row.nm_kategori} ?`,
                        subTitle:"You can't undo this operation",
                        onConfirm: () => {onDelete(row)}
                    })
                   // onDelete(row.id)
                    }}>
                      <DeleteIcon fontSize="small"/>
                </Control.ActionButton>    
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TblContainer>
      <TblPagination />
      <Control.Notification
         notif={notif}
         setNotif={setNotif}
         />

      <Control.ConfirmDialog
        confirmDialog={confirmDialog}
        setConfirmDialog={setConfirmDialog}
      />
       <Control.popup
           title={titleForm}
           openPopup = {openPopup}
           setOpenPopup = {setOpenPopup}
           size = "sm"
           >
        <FormProduct
          setAfterSave = {setAfterSave}
        />
       </Control.popup>
    </MainCard>


    );
};

export default ProdukKategori;
