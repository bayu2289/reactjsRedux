import React,{useState,useEffect} from 'react';
import { makeStyles } from '@material-ui/styles';
import Paper from '@material-ui/core/Paper';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Control from './../../ui-component/control';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import * as LocalStorege from './../../store/localStorage'
import Api from './../../axios/Api'

const validationSchema = yup.object({
    namaProduk: yup
      .string('Enter your Nama Produk')
      .required('Nama Produk is required'),
 
  });

  const useStyles = makeStyles((theme) => ({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 400,
      },
    },
    paper: {
        padding: theme.spacing(1),
        textAlign: 'left',
        color: theme.palette.text.secondary,
      },
      formControl: {
        margin: theme.spacing(1),
        minWidth: 400,
      },
  }));

 const Form = (props) => {
    const classes = useStyles();
    const [values,setValues] = useState('');
    const [dataKategori,setDataKategori] = useState([]);

    const getDataKategori = async () => {
        console.log(LocalStorege.getTokenAuth());
        //await ApiStaging.get('get-item-produk', {
        await Api.get('kategori/', {
          headers: {
            'Content-Type': 'application/json',
            'Accept':'application/json',
            'Authorization': 'Bearer ' + LocalStorege.getTokenAuth()
          },
        //  params: {
         //   branch_id: 1,
         //   page: 1
        //  }
        })
        .then((res) => {
     
        //console.log("kategori",res)
        setDataKategori(res.data)
    
        })
        .catch((error) => {
          console.error(error)
        })
    }

    useEffect(() => {
        getDataKategori();
        
        console.log("kategorii",dataKategori)
   
     },[]);

    console.log("tes",props)
    const formik = useFormik({
        initialValues: {
          namaProduk: '',
          email: 'foobar@example.com',
          password: 'foobar',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            console.log(values)
          alert(JSON.stringify(values, null, 2));
        },
      });

      const handleInputChange = e => {
      
        const {name,value} = e.target
        setValues({
          ...values,
          [name] : value
        })
       
       }
    return (
        <div>
          <form className={classes.root} onSubmit={formik.handleSubmit}>
          <Paper className={classes.paper}>
          <TextField
            fullWidth
            id="namaProduk"
            name="namaProduk"
            label="Nama Produk"
            value={formik.values.namaProduk}
            onChange={formik.handleChange}
            error={formik.touched.namaProduk && Boolean(formik.errors.namaProduk)}
            helperText={formik.touched.namaProduk && formik.errors.namaProduk}
            />

            <FormControl className={classes.formControl}>
            <InputLabel htmlFor="age-native-helper">Kategori</InputLabel>
                 <Select
            id="kategori"
            name="kategori"
            label="kategori"
                value={formik.values.kategori}
                onChange={formik.handleChange}
                >
                  {
                            dataKategori.map(
                            item=>(<MenuItem key={item.id} value={item.id}>{item.nm_kategori}</MenuItem>)
                            )
                        }
             </Select>
             </FormControl>
       
            <TextField
            fullWidth
            id="modal"
            name="modal"
            label="Modal"
            value={formik.values.modal}
            onChange={formik.handleChange}
            error={formik.touched.modal && Boolean(formik.errors.modal)}
            helperText={formik.touched.modal && formik.errors.modal}
            />
            <TextField
            fullWidth
            id="hargaProduk"
            name="hargaProduk"
            label="Harga"
            value={formik.values.hargaProduk}
            onChange={formik.handleChange}
            error={formik.touched.hargaProduk && Boolean(formik.errors.hargaProduk)}
            helperText={formik.touched.hargaProduk && formik.errors.hargaProduk}
            />

            <TextField
            fullWidth
            id="stok"
            name="stok"
            label="Stok"
            value={formik.values.stok}
            onChange={formik.handleChange}
            error={formik.touched.stok && Boolean(formik.errors.stok)}
            helperText={formik.touched.stok && formik.errors.stok}
            />
            <TextField
            fullWidth
            id="minStok"
            name="minStok"
            label="Min Stok"
            value={formik.values.minStok}
            onChange={formik.handleChange}
            error={formik.touched.minStok && Boolean(formik.errors.minStok)}
            helperText={formik.touched.minStok && formik.errors.minStok}
            />
            <TextField
            fullWidth
            id="satuan"
            name="satuan"
            label="Satuan"
            value={formik.values.satuan}
            onChange={formik.handleChange}
            error={formik.touched.satuan && Boolean(formik.errors.satuan)}
            helperText={formik.touched.satuan && formik.errors.satuan}
            />
              <FormControl className={classes.formControl}>
            <InputLabel htmlFor="age-native-helper">Outlet</InputLabel>
                 <Select
            id="outlet"
            name="outlet"
            label="outlet"
                value={formik.values.outlet}
                onChange={formik.handleChange}
                >
                  {
                            dataKategori.map(
                            item=>(<MenuItem key={item.id} value={item.id}>{item.nm_kategori}</MenuItem>)
                            )
                        }
             </Select>
             </FormControl>
        
         

          </Paper>
          <Paper className={classes.paper}>
                
          <Button color="primary" variant="contained" type="submit">
            Submit
            </Button>
          </Paper>
       
        </form>
        </div>
    )
}
export default Form;