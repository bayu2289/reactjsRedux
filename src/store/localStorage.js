const KEYS={
    produk:'produk',
    produkId:'produkId',
    auth: 'auth',
    authId: 'authId'
}

export function copyFromDb(data){
    localStorage.removeItem(KEYS.produk)
    console.log('copyFromDB',data);
     localStorage.setItem(KEYS.produk,JSON.stringify(data))
 }
 
export function cleareDb(){
  localStorage.clear();
}

export function loginAuth(data){
  console.log(data);
  localStorage.setItem(KEYS.auth,JSON.stringify(data))
}

export function getLoginAuth() {
  let data = JSON.parse(localStorage.getItem(KEYS.auth));
  return data
}

export function getTokenAuth() {
  let data = JSON.parse(localStorage.getItem(KEYS.auth));
  if(data == null){
    return false
  }
  return data.token
}

export function clearLocalStorage(){
  localStorage.clear()
}