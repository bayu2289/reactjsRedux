import { createStore,applyMiddleware } from 'redux';
import reducer from './reducer';
import thunk from 'redux-thunk'

//-----------------------|| REDUX - MAIN STORE ||-----------------------//

const store = createStore(reducer,applyMiddleware(thunk));
//const store = createStore(reducer,applyMiddleware(thunk),{},window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
export { store };
