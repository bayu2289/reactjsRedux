import { combineReducers } from 'redux';

// reducer import
import customizationReducer from './customizationReducer';
import userReducer from '../redux/reducers/userReducer';
import productReducer from '../redux/reducers/productReducer';
import kategoriReducer from '../redux/reducers/kategoriReducer';


//-----------------------|| COMBINE REDUCER ||-----------------------//

const reducer = combineReducers({
    customization: customizationReducer,
    allProduct: productReducer,
    searchProduk: productReducer,
    users: userReducer,
    allKategori : kategoriReducer,
});

export default reducer;
