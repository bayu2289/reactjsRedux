import React from 'react';
import { Redirect, Switch,useHistory } from 'react-router-dom';

// routes
import MainRoutes from './MainRoutes';
import AuthenticationRoutes from './AuthenticationRoutes';

// project imports
import config from './../config';


// data from local storage
import * as Localstorage from './../store/localStorage'



//-----------------------|| ROUTING RENDER ||-----------------------//


const Routes = () => {
  
    //console.log(Localstorage.getTokenAuth())
    console.log(Localstorage.getTokenAuth())
    let history = useHistory();
    // if(Localstorage.getTokenAuth() == false){
    // //     // <Redirect exact from="/" to={config.defaultPath} />
    // //     console.log("lewat router boss")
    //     // history.push(config.defaultPath)
    //     <Redirect exact from="/" to={config.defaultPath} />
    //     }
    // else{
    //     <Redirect exact from="/" to={config.afterLoginPath} />
    // }
    return (
       
        <Switch>
           
             {/* <Redirect exact from="/pages/login/" to="/" /> */}
             <Redirect exact from="/" to={config.afterLoginPath} />
            <React.Fragment>
                {/* Routes for authentication pages */}
                <AuthenticationRoutes />

                {/* Routes for main layouts */}
    
                 <MainRoutes />
            </React.Fragment>
        </Switch>
    );
};

export default Routes;
