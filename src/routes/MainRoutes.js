import React, { lazy } from 'react';
import { Route, Switch, useLocation, useHistory } from 'react-router-dom';
import PrivateRoute from './PrivateRoutes'

// project imports
import MainLayout from './../layout/MainLayout';
import Loadable from '../ui-component/Loadable';


// data from local storage
import * as Localstorage from './../store/localStorage'


// project imports
import config from './../config';

// dashboard routing
const DashboardDefault = Loadable(lazy(() => import('../views/dashboard/Default')));

// utilities routing
const UtilsTypography = Loadable(lazy(() => import('../views/utilities/Typography')));
const UtilsColor = Loadable(lazy(() => import('../views/utilities/Color')));
const UtilsShadow = Loadable(lazy(() => import('../views/utilities/Shadow')));
const UtilsMaterialIcons = Loadable(lazy(() => import('../views/utilities/MaterialIcons')));
const UtilsTablerIcons = Loadable(lazy(() => import('../views/utilities/TablerIcons')));

// sample page routing
const SamplePage = Loadable(lazy(() => import('../views/sample-page')));
const Tes =  Loadable(lazy(() => import('../views/tes')));
const ProdukKategori  =  Loadable(lazy(() => import('../views/produkKategori')));


//-----------------------|| MAIN ROUTING ||-----------------------//

const MainRoutes = () => {
    const location = useLocation();
    console.log(location);
   // console.log(Localstorage.getTokenAuth())
    let history = useHistory();

    return (
       
        <Route
            path={[
                '/dashboard',

                '/utils/util-typography',
                '/utils/util-color',
                '/utils/util-shadow',
                '/icons/tabler-icons',
                '/icons/material-icons',

                '/sample-page',
                '/tes',
                '/produkKategori'
            ]}
            
        >
          
            <MainLayout>
                <Switch location={location} key={location.pathname}>
                    <PrivateRoute path="/dashboard" component={DashboardDefault} />

                    <Route path="/utils/util-typography" component={UtilsTypography } />
                    <Route path="/utils/util-color" component={UtilsColor} />
                    <Route path="/utils/util-shadow" component={UtilsShadow} />
                    <Route path="/icons/tabler-icons" component={UtilsTablerIcons} />
                    <Route path="/icons/material-icons" component={UtilsMaterialIcons} />

                    <Route path="/sample-page" component={SamplePage} />
                    <PrivateRoute path="/tes" component={Tes} />
                    <PrivateRoute path="/produkKategori" component={ProdukKategori} />
                </Switch>
            </MainLayout>
            
         
        </Route>
    );
};

export default MainRoutes;
