import React,{useEffect,useState} from 'react'
import { Redirect, Route } from 'react-router-dom'
// data from local storage
import * as Localstorage from './../store/localStorage'


// project imports
import config from './../config';


const PrivateRoute = ({ component: Component, ...rest }) => {

    const [login,setLogin] = useState(true);
  // Add your own authentication on the below line.
  
// let isLoggedIn 
    useEffect(() => {
    
        setLogin(Localstorage.getTokenAuth())
       
        if(login == undefined){
            setLogin(false)
        }
    
        //console.log("login",login)
    },[rest,login,Component]);
  //const isLoggedIn = Localstorage.getTokenAuth()
 

  return (
    <Route
      {...rest}
      render={props =>
       
        login != false  ? ( 
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: 'pages/login/login3', state: { from: props.location } }} />
        // <Redirect exact from="/" to={config.defaultPath} /> 
        
        )
      }
    />
  )
}

export default PrivateRoute