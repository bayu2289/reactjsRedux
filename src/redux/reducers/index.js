import { combineReducers } from "redux";
import { productReducer } from "./productReducer";
import { userReducer } from "./userReducer";
import { kategoriReducer} from "./kategoriReducer"

const reducers = combineReducers({
    allProducts: productReducer,
    userData : userReducer,
    allKategori: kategoriReducer,
});

export default reducers;