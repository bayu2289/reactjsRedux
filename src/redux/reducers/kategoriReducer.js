import { ActionType } from "../contants/action-types"

const initialState = {
    kategori:[],
    editkategori:[]
}
export const kategoriReducer = (state = initialState,{type,payload}) =>{
    switch(type){
        case ActionType.SET_KATEGORI:
            return {...state, kategori: payload};
        case ActionType.SELECTED_KATEGORI:
            return {...state, editkategori: payload};
        case ActionType.SEARCH_KATEGORI:
            return {...state, kategori: payload};
        default:
            return state;
    }
}
export default kategoriReducer;