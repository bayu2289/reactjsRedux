import { ActionType } from "../contants/action-types"
import Api from "./../../axios/Api"
import * as LocalStorege from "./../../store/localStorage"


export const setKategori = () => {

    return (dispatch) => {
        Api.get('kategori/', {
            headers: {
              'Content-Type': 'application/json',
              'Accept':'application/json',
              'Authorization': 'Bearer ' + LocalStorege.getTokenAuth()
            },
          //  params: {
           //   branch_id: 1,
           //   page: 1
          //  }
          })
          .then((res) => {
 
          //  console.log("dispact",res.data);
           // setAllDataProduct(res.data)
           dispatch({
                type: ActionType.SET_KATEGORI,
                payload: res.data
            })
     
      
          })
          .catch((error) => {
            console.error(error)
          })
    }


}

export const selectedKategori = (kategori) => {
    return {
        type: ActionType.SELECTED_KATEGORI,
        payload: kategori,
    }
}

export const searchKategori = (kategori, search) => {
    return {
        type : ActionType.SEARCH_KATEGORI,
        payload: kategori.filter(x=>x.nm_kategori.toUpperCase().includes(search.toUpperCase()))
    }
 }