 import { ActionType } from "../contants/action-types"
 export const setProducts = (products) => {
     return {
         type: ActionType.SET_PRODUCTS,
         payload: products,
     }
 }

 export const selectedProduct = (products) => {
     return {
         type: ActionType.SELECTED_PRODUCT,
         payload: products,
     }
 }

 export const searchProduct = (product, search) => {
    return {
        type : ActionType.SEARCH_PRODUCT,
        payload: product.filter(x=>x.nama_produk.toUpperCase().includes(search.toUpperCase()))
    }
 }